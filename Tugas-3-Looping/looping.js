// LOOPING PERTAMA
console.log('-----------------------')
console.log('----LOOPING PERTAMA----')
console.log('-----------------------')

var hitung = 2;
while(hitung < 22) {
    console.log(hitung + ' - I Love Coding');
    hitung += 2;
}

// LOOPING KEDUA
console.log('')
console.log('-----------------------')
console.log('-----LOOPING KEDUA-----')
console.log('-----------------------')

var turun = 22;
while(turun > 0) {
    hitung += turun;
    turun -= 2;
    console.log(turun + ' - I Love Coding')
}

// LOOPING FOR
console.log('')
for(var naik = 1; naik < 21; naik ++) {
    if(naik % 2 == 0) {
        console.log(naik + ' - Berkualitas')
    } else if (naik % 3 == 0 && naik % 2 ==1) {
        console.log(naik + ' - I Love Coding')
    }
    else {
        console.log(naik + ' - Santai')
    }
}

// LOOPING PERSEGI PANJANG
console.log('')
for(var kotak = 1; kotak < 5; kotak ++) {
    console.log('########')
}

// LOOPING TANGGA
console.log('')

for(var tangga = 1; tangga <=7; tangga ++) {
    if(tangga == 1) {
        console.log('#')
    } else if(tangga == 2) {
        console.log('##')
    } else if(tangga == 3) {
        console.log('###')
    } else if(tangga == 4) {
        console.log('####')
    } else if(tangga == 5) {
        console.log('#####')
    } else if(tangga == 6) {
        console.log('######')
    } else {
        console.log('#######')
    }
    
}

// LOOPING CATUR
console.log('')
for(var catur = 1; catur < 9; catur ++) {
    if(catur % 2 == 1) {
        console.log(' # # # # #')
    }
    else {
        console.log('# # # # # ')
    }
}