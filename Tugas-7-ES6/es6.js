// Menggunakan ES6 sebagai penulisan program
golden = () => {
    console.log('this is golden !!')
}

golden()

// Objek Literal Function dengan Arrow
const literal = {
    fullName(a , b) {
        return a + ' ' + b
    }
}
console.log(literal.fullName('William' , 'Imoh'))

// Destructing Parameter dengan ES6
let magicianName = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName , lastName , destination , occupation , spell } = magicianName

console.log(magicianName)

// Array Spreading dengan ES6
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]
console.log(combined)

// Template Literal dengan ES6
const planet = "earth"
const view = "glass"

const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elite ${planet} do elusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim venim `

console.log(before)
