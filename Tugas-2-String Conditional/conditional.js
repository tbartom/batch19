// Tugas Conditional

var nama = "Danu"
var peran = "Guard"

if (nama == "Ayu") {
    console.log("Halo " + nama + " , Pilih peranmu untuk memulai game")
} else if (nama == "Budi") {
    console.log("Selamat datang di dunia werewolf, " + nama)
    if(peran == "Penyihir") {
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf")
    } else if(peran == "Guard") {
        console.log("Halo Guard " + nama + ", kamu akan membantu temanmu dari serangan werewolf")
    } else if(peran == "Werewolf") {
        console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam !!") 
    }
} else if (nama == "Cinta") {
    console.log("Selamat datang di dunia werewolf, " + nama)
    if(peran == "Penyihir") {
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf")
    } else if(peran == "Guard") {
        console.log("Halo Guard " + nama + ", kamu akan membantu temanmu dari serangan werewolf")
    } else if(peran == "Werewolf") {
        console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam !!") 
    }
} else if (nama == "Danu") {
    console.log("Selamat datang di dunia werewolf, " + nama)
    if(peran == "Penyihir") {
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf")
    } else if(peran == "Guard") {
        console.log("Halo Guard " + nama + ", kamu akan membantu temanmu dari serangan werewolf")
    } else if(peran == "Werewolf") {
        console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam !!") 
    }

}

var hari = 21
var bulan = 12
var tahun = 1945

if (hari >= 1 || hari <= 31 || bulan >= 1 || bulan <= 12 || tahun >= 1900 || tahun <= 2200) {
    switch(bulan) {
        case 1: { console.log(hari +" "+" Januari "+ tahun ); break; }
        case 2: { console.log(hari +" "+" Februari "+ tahun ); break; }
        case 3: { console.log(hari +" "+" Maret "+ tahun ); break; }
        case 4: { console.log(hari +" "+" April "+ tahun ); break; }
        case 5: { console.log(hari +" "+" Mei "+ tahun ); break; }
        case 6: { console.log(hari +" "+" Juni "+ tahun ); break; }
        case 7: { console.log(hari +" "+" Juli "+ tahun ); break; }
        case 8: { console.log(hari +" "+" Agustus "+ tahun ); break; }
        case 9: { console.log(hari +" "+" September "+ tahun ); break; }
        case 10: { console.log(hari +" "+" Oktober "+ tahun ); break; }
        case 11: { console.log(hari +" "+" November "+ tahun ); break; }
        case 12: { console.log(hari +" "+" Desember "+ tahun ); break; }}
    } else {
        console.log("Masukkan data yang benar")
    }
