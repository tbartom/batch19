// Tugas String

var word = 'JavaScript'
var second = 'is'
var third = 'awesome'
var fourth = 'and'
var fifth = 'I'
var sixth = 'love'
var seventh = 'it'

console.log(word +" "+ second +" "+ third +" "+ fourth +" "+ fifth +" "+ sixth +" "+ seventh)
console.log("")
console.log("####################")
console.log("")

var sentences = "I am going to be React Native Developer"
var FirstWord = sentences[0]
var SecondWord = sentences[2] + sentences[3]
var ThirdWord = sentences[5] + sentences[6] + sentences[7] + sentences[8] +sentences[9]
var FourthWord = sentences[11] + sentences[12]
var FifthWord = sentences[14] + sentences[15]
var SixthWord = sentences[17] + sentences[18] + sentences[19] + sentences[20] + sentences[21]
var SeventhWord = sentences[23] + sentences[24] + sentences[25] + sentences[26] + sentences[27] +sentences[28]
var EigthWord = sentences[30] + sentences[31] + sentences[32] + sentences[33] + sentences[34] + sentences[35] + sentences[36] + sentences[37] + sentences[38]

console.log('First Word: ' + FirstWord);
console.log('Second Word: ' + SecondWord);
console.log('Third Word: ' + ThirdWord);
console.log('Fourth Word: ' + FourthWord);
console.log('Fifth Word: ' + FifthWord);
console.log('Sixth Word: ' + SixthWord);
console.log('Seventh Word: ' + SeventhWord);
console.log('Eight Word: ' + EigthWord)

console.log("")
console.log("####################")
console.log("")

var sentence3 = 'wow Javascript is cool'
var FirstWord3 = sentence3.substring(0, 3);
var SecondWord3 = sentence3.substring(4, 14);
var ThirdWord3 = sentence3.substring(15, 17);
var FourthWord3 = sentence3.substring(18, 22)

console.log('First Word: ' + FirstWord3 + ', with length: ' + FirstWord3.length);
console.log('Second Word: ' + SecondWord3 + ', with length: ' + SecondWord3.length);
console.log('Third Word: ' + ThirdWord3 + ', with length: ' + ThirdWord3.length);
console.log('Fourth Word: ' + FourthWord3 + ', with length: ' + FourthWord3.length)