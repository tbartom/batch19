console.log('--------- No. 1 ----------')

function range(startNum , finishNum) {
    var rangeArr = []

    if(startNum > finishNum) {
        var rangeLength = startNum - finishNum + 1
        for ( var i = startNum; i <= finishNum; i ++) {
            rangeArr.push(startNum - i)
        }
    } else if(startNum < finishNum) {
        var rangeLength = finishNum - startNum + 1
        for ( var i = startNum; i <= finishNum; i ++) {
            rangeArr.push(startNum + i)
        }
    } else if( !startNum || !finishNum) {
        return -1
    }
    return rangeArr
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

console.log('--------- No. 2 ----------')

function rangeWithStep(startNum , finishNum , step) {
    var rangeArr = []

    if(startNum > finishNum) {
        var currentNum = startNum
        for ( var i = 0; currentNum >= finishNum; i ++) {
            rangeArr.push(currentNum)
            currentNum -= step
        }
    } else if(startNum < finishNum) {
        var currentNum = startNum
        for ( var i = 0; currentNum <= finishNum; i ++) {
            rangeArr.push(currentNum)
            currentNum += step
        }
    } else if( !startNum || !finishNum || !step) {
        return -1
    }
    return rangeArr
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log('--------- No. 3 ----------')

function sum(firstNum , lastNum , step) {
    var number = []
    if(firstNum > lastNum) {
        var rangeSum = firstNum - lastNum + 1
        for(var i = 0; i < rangeSum; i += step) {
            number.push(firstNum - i)
        } 
    } else {
        var rangeSum = lastNum - firstNum + 1
        for(var i = 0; i < rangeSum; i += step) {
            number.push(firstNum + i)
            }
        }
    var jumlah = 0
    for(var i = 0; i < number.length; i ++) {
        jumlah += number[i]
    }
    return jumlah
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log('--------- No. 4 ----------')

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
var proses1 = 0
var proses2 = input.length
var output = ''

function dataHandling (input) {
    for(proses1; proses1 < proses2; proses1++) {
        output += 'Nomor ID: ' + input [proses1] [0] + '\n' + 'Nama Lengkap: ' + input [proses1] [1] + '\n' + 'TTL: ' + input [proses1] . slice (2,4) . join (' ') + '\n' + 'Hobi: ' + input [proses1] [4] + '\n' + '\n'
    }   
    return output;
}

console.log(dataHandling (input))

console.log('--------- No. 5 ----------')
var input = "           "
var proses = (input.length)-1
var output = ''

function balikKata(input) {
    for(proses; proses >= 0; proses--) {
        output += input[proses]
        }
    return output
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers")) 

console.log('--------- No. 6 ----------')

var input1 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
var proses2 = input1[3].split('/');
var proses3 = proses2 [1];
var e = proses2;
var a = parseInt(proses2[0])
var b = parseInt(proses2[1])
var c = parseInt(proses2[2])
var d = [a, b, c]


function dataHandling (input1) {
    input1.splice(4,1, "Pria", "SMA Internasional Metro")
    input1.splice(0,3, "0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    console.log(input1)
    if (proses3 == 1) {
        console.log('Januari');
    } else if (proses3 == 2) {
        console.log('Februari');
    } else if (proses3==3) {
        console.log('Maret');
    } else if (proses3==4) {
        console.log('April');
    } else if (proses3==5) {
        console.log('Mei');
    } else if (proses3==6) {
        console.log('Juni');
    } else if (proses3==7) {
        console.log('Juli');
    } else if (proses3==8) {
        console.log('Agustus');
    } else if (proses3==9) {
        console.log('September');
    } else if (proses3==10) {
        console.log('Oktober');
    } else if (proses3==11) {
        console.log('Nopember');
    } else if (proses3==12) {
        console.log('Desember');
    }
    d.sort(function(a, b){return b-a})
    console.log(d)
    e.sort(function(a, b){return b-a})
    console.log(e.join('-'))
    console.log(input1[1].slice(0,15))
}

console.log(dataHandling(input1))