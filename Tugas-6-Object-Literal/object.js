console.log('------------ soal 1 -------------')
var input = [
    ["Bruce" , "Banner" , "male" , 1975],
    ["Natasha", "Romanoff", "female"],
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023] 
]
var now = new Date()
var thisYear = now.getFullYear()

function arraytoObject(input) {
    for(var i = 0; i < 4; i ++) {
        var object = {}
        if(input[i][3] < thisYear) {
            age = thisYear - input[i][3]
        } else {
            age = "Invalid Birth Year"
        }
        object.firstname = input[i][0]
        object.lastname = input[i][1]
        object.gender = input[i][2]
        object.age = age

        var output = (i + 1) + ' . ' + object.firstname + object.lastname + ' : ' + object.age

        console.log(output)
        console.log(object)
        console.log('\n')
    }
}

arraytoObject(input)
// console.log(thisYear)

console.log('------------ soal 2 -------------')


function shoppingTime(memberId , money) {
    var shopping = {}
    var barang = [
                    ['Sepatu Stacattu', 1500000],
                    ['Baju Zoro', 500000],
                    ['Baju H&N', 250000],
                    ['Sweater Uniklooh', 175000],
                    ['Casing Handphone', 50000]
                 ]
    
    if(memberId === undefined && money === undefined) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }
    
    if(memberId === '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else {
        shopping.memberId = memberId
    }
    
    if(money <= 50000 ) {
        return 'Mohon maaf, uang tidak cukup'
    } else {
      shopping.money = money
    }
    
    var jumlahHargaBarang = 0
    shopping.listPurchased = []
    for(let i = 0; i < barang.length; i++) {
      
        if(money > barang[i][1]) {
            shopping.listPurchased.push(barang[i][0])
            jumlahHargaBarang += barang[i][1]
        }
        shopping.changeMoney = money - jumlahHargaBarang
    }
    return shopping
  }
  
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  console.log('------------ soal 3 -------------')

  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var angkot = [{},{}]
    var i = 0
    var asal = ''
    var tujuan = ''
  
    for (i; i < arrPenumpang.length; i ++) {
        var j = 0
        for (j; j< arrPenumpang[i].length; j ++) {
            switch (j) {
                case 0: {
                    angkot[i].penumpang = arrPenumpang[i][j]
                    break
                } case 1: {
                    angkot[i].naikDari = arrPenumpang[i][j]
                    angkot[i].tujuan = arrPenumpang[i][j+1]
                    break
                } case 2: {
                    asal = arrPenumpang[i][j-1]
                    tujuan = arrPenumpang[i][j]
                    var jarak = 0
                    for (var k = 0; k < rute.length; k ++) {
                        if (rute[k] === asal) {
                            for (var l = k+1; l < rute.length; l ++) {
                                jarak += 1
                                if (rute[l] === tujuan) {
                                    var bayar = jarak * 2000
                                    angkot[i].bayar = bayar
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
  return angkot;
  }
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
  